<?php

date_default_timezone_set('America/Sao_Paulo');

use PHPMailer\PHPMailer\PHPMailer;

require '../vendor/autoload.php';


define('BOT_TOKEN', getenv('BOT_TOKEN'));
define('API_URL', 'https://api.telegram.org/bot' . BOT_TOKEN . '/');
define('API_NASA_SECRET', getenv('API_NASA_SECRET'));

//API DE CRON
define('API_ATRIGGER_URL', 'https://api.atrigger.com/v1/tasks/');
define('API_ATRIGGER_KEY', getenv('API_ATRIGGER_KEY'));
define('API_ATRIGGER_SECRET', getenv('API_ATRIGGER_SECRET'));

//Usei o SMTP DO GOOGLE
define('GUSERNAME', getenv('GUSERNAME'));
define('GPASSWORD', getenv('GPASSWORD'));



function exec_curl_request($handle)
{
    $response = curl_exec($handle);

    if ($response === false) {
        $errno = curl_errno($handle);
        $error = curl_error($handle);
        error_log("Curl returned error $errno: $error\n");
        curl_close($handle);
        return false;
    }

    $http_code = intval(curl_getinfo($handle, CURLINFO_HTTP_CODE));
    curl_close($handle);

    if ($http_code >= 500) {
        // do not wat to DDOS server if something goes wrong
        sleep(10);
        return false;
    } else if ($http_code != 200) {
        $response = json_decode($response, true);
        error_log("Request has failed with error {$response['error_code']}: {$response['description']}\n");
        if ($http_code == 401) {
            throw new Exception('Invalid access token provided');
        }
        return false;
    } else {
        $response = json_decode($response, true);
        if (isset($response['description'])) {
            error_log("Request was successful: {$response['description']}\n");
        }
        $response = $response['result'];
    }

    return $response;
}

function apiRequest($method, $parameters, $cron = false)
{

    if (!is_string($method)) {
        error_log("Method name must be a string\n");
        return false;
    }

    if (!$parameters) {
        $parameters = array();
    } else if (!is_array($parameters)) {
        error_log("Parameters must be an array\n");
        return false;
    }

    foreach ($parameters as $key => &$val) {
        if (!is_numeric($val) && !is_string($val)) {
            $val = json_encode($val);
        }
    }

    if (!$cron) {
        $url = API_URL . $method . '?' . http_build_query($parameters);
    } else {
        switch ($method) {
            case 'create':

                $email = "&email={$parameters["email"]}";

                $telegram_url = urlencode(WEBHOOK_URL . "index.php/sendMessage?chat_id={$parameters["tag_id"]}&text=notification&tipo={$parameters["tipo"]}&cron=true$email&nome={$parameters["nome"]}");
                $url = API_ATRIGGER_URL . $method . '?key=' . API_ATRIGGER_KEY . '&secret=' . API_ATRIGGER_SECRET . '&' . http_build_query($parameters) . '&url=' . $telegram_url;
                break;

            case 'delete':
                $url = API_ATRIGGER_URL . $method . '?key=' . API_ATRIGGER_KEY . '&secret=' . API_ATRIGGER_SECRET . '&' . http_build_query($parameters);
                break;
        }

    }

    $handle = curl_init($url);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($handle, CURLOPT_TIMEOUT, 60);

    return exec_curl_request($handle);
}

function hazardsAsteroids($dataSemHora,$email = false)
{
    $url = "https://api.nasa.gov/neo/rest/v1/feed?start_date=$dataSemHora&end_date=$dataSemHora&detailed=false&api_key=".API_NASA_SECRET."";
    $handle = curl_init($url);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($handle, CURLOPT_TIMEOUT, 60);

    $mensagem = curl_exec($handle);
    curl_close($handle);

    $hazardsAsteroids = "";

    $titulo =  "";

    $result = json_decode($mensagem, true);
    $neos = $result["near_earth_objects"][$dataSemHora];

    foreach ($neos as $key => $neo) {
        if ($neo["is_potentially_hazardous_asteroid"]) {

            /*
            Essa parte está 'mal identada' por causa da tag <pre>,por isso não dei espaçamentos adicionais,
            o telegram tem um número restrito de tags e entre elas não está a tag <br>
            */
            if(!$email){
            $hazardsAsteroids .= "<pre>
</pre><b>{$neo["name"]}</b>
Nome: {$neo["name"]}
Diâmetro estimado (em Km): {$neo["estimated_diameter"]["kilometers"]["estimated_diameter_min"]}
Distância da Terra (em Km): {$neo["close_approach_data"][0]["miss_distance"]["kilometers"]}
Velocidade relativa à Terra (em Km/h): {$neo["close_approach_data"][0]["relative_velocity"]["kilometers_per_hour"]}
Detalhes do Asteróide: <a href='{$neo["nasa_jpl_url"]}'>Saiba Mais</a>
                ";
            }else{
                $titulo = "<p><h2>Esses são os asteróides que ameaçam a terra no dia de hoje:<h2></p>";

                $hazardsAsteroids .= "<b>{$neo["name"]}</b> <br>
                Nome: {$neo["name"]} <br>
                Diâmetro estimado (em Km): {$neo["estimated_diameter"]["kilometers"]["estimated_diameter_min"]} <br>
                Distância da Terra (em Km): {$neo["close_approach_data"][0]["miss_distance"]["kilometers"]} <br>
                Velocidade relativa à Terra (em Km/h): {$neo["close_approach_data"][0]["relative_velocity"]["kilometers_per_hour"]} <br>
                Detalhes do Asteróide: <a href='{$neo["nasa_jpl_url"]}'>Saiba Mais</a> <br>
                <p></p>";
            }
        }
    }

    $hazardsAsteroids = $titulo.$hazardsAsteroids;
    return $hazardsAsteroids;
}

function processCallback($callback)
{

    $nome = $callback['from']['first_name'];
    $mensagem = $callback['message']['text'];
    $chat_id = $callback["message"]["chat"]["id"];

    $dataIso = date("c",strtotime("tomorrow 00:00:00"));

    if ($mensagem == "Gostaria de receber alertas diários?") {
        if ($callback["data"] == "Sim") {

            requestNotificacao($chat_id);

        } else {
            apiRequest("sendMessage", array("chat_id" => $chat_id, "text" => "Ok,você não será notificado."));
        }
    }

    if ($mensagem == "Escolha o meio de notificação:") {
        if ($callback["data"] == "Notificação") {
            requestHorario($chat_id, 1);
        } elseif ($callback["data"] == "E-mail") {
            apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => "Digite o seu e-mail:", "reply_markup" => array(
                'force_reply' => true,
            )));
        } elseif ($callback["data"] == "Notificação e E-mail") {
            apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => "Digite o e-mail de destino:", "reply_markup" => array(
                'force_reply' => true,
            )));
        }
    }

    if ($mensagem == "Em qual horário gostaria de receber os alertas?") {

        //Como o callback do telegram só permite string,separei tudo com vírgulas
        $result = explode(",", $callback["data"]);

        //Formatação para envio na api ATRIGGER
        $horario = $result[0];
        //Formatação para envio de mensagem no telegram
        $horarioFormatado = str_pad($result[0], 3, 0, STR_PAD_LEFT);

        $tipo = $result[1];
        $email = $result[2];

        apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => "Todo dia às $horarioFormatado você será avisado sobre os asteróides!"));

        //Para evitar duplicação de cron,sempre deleto crons anteriores,a api ATRIGGER não tem opção de alteração.
        apiRequest("delete", array("tag_id" => $chat_id), true);
        apiRequest("create", array("timeslice" => "{$horario}our", "count" => -1, "tag_id" => $chat_id, "first" => $dataIso, "tipo" => $tipo, "email" => $email, "nome" => $nome), true);
        

    }

}

function processMessage($message)
{

    $message_id = $message['message_id'];
    $chat_id = $message['chat']['id'];

    if (isset($message['text'])) {

        $text = $message['text'];

        if (strpos($text, "/iniciar") === 0) {

            //Forneci um botão para facilitar a vida do usuário
            apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => 'Seja bem-vindo!', 'reply_markup' => array(
                'keyboard' => array(array('Algum asteróide oferece perigo ao planeta Terra na data de hoje?')),
                'one_time_keyboard' => true,
                'resize_keyboard' => true)));

        } else if ($text === "Algum asteróide oferece perigo ao planeta Terra na data de hoje?") {

            $dataSemHora = date('Y-m-d', time());

            $hazardsAsteroids = hazardsAsteroids($dataSemHora);

            if ($hazardsAsteroids == "") {

                $hazardsAsteroids = "Não, fique tranquilo ;-)";

            } else {

                apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => "Sim!"));

            }

            apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => $hazardsAsteroids, "parse_mode" => "html", "disable_web_page_preview" => true));

            apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => "Gostaria de receber alertas diários?", "reply_markup" => array(
                'inline_keyboard' => array(
                    array(
                        array("text" => "Sim", "callback_data" => "Sim"),
                        array("text" => "Não", "callback_data" => "Não"),
                    ),
                ),
            )));

        } else if (strpos($text, "/encerrar") === 0) {

            apiRequest("sendMessage", array("chat_id" => $chat_id, "text" => "Até logo!"));

        } else if ($message["reply_to_message"]["text"] == "Digite o seu e-mail:") {

            requestHorario($chat_id, 2, $message["text"]);

        } else if ($message["reply_to_message"]["text"] == "Digite o e-mail de destino:") {

            requestHorario($chat_id, 3, $message["text"]);

        } else if (strpos($text, "/amanha") === 0) {

            $dataSemHora = date("Y-m-d", strtotime("+1 day"));
            $hazardsAsteroids = hazardsAsteroids($dataSemHora);

            if ($hazardsAsteroids == "") {
                $hazardsAsteroids = "Não há nenhum asteróide perigoso hoje, fique tranquilo ;-)";
            }

            apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => $hazardsAsteroids, "parse_mode" => "html", "disable_web_page_preview" => true));

        } else if (strpos($text, "/reagendar") === 0) {

            requestNotificacao($chat_id);

        } else if (strpos($text, "/silenciar") === 0) {

            apiRequest("delete", array("tag_id" => $chat_id), $chat_id);
            apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => "Você cancelou o recebimento de notificações!"));

        } else {

            apiRequest("sendMessage", array('chat_id' => $chat_id, "reply_to_message_id" => $message_id, "text" => 'Não entendi o que você disse'));

        }

    }
}

//Definição da url de webhhok
define('WEBHOOK_URL', 'https://botapocalipse.herokuapp.com/7D6DF1652A1FFBBDEE9EB6CD1446D/');
if (php_sapi_name() == 'cli') {
    apiRequest('setWebhook', array('url' => isset($argv[1]) && $argv[1] == 'delete' ? '' : WEBHOOK_URL));
    exit;
}

//Recebimento dos dados
$content = file_get_contents("php://input");
$update = json_decode($content, true);

//Se vier através de uma requisição do CRON
if (isset($_SERVER['QUERY_STRING']) && !isset($update)) {
    $update = array();
    parse_str($_SERVER['QUERY_STRING'], $update);
}

if (!$update) {
    exit;
}

//Tratamento dos dados enviados pelo cron
if (isset($update["cron"])) {
    // $dataSemHora = date("Y-m-d");
    $dataSemHora = date("Y-m-d", strtotime("now"));
        

    if ($hazardsAsteroids == "") {

        $hazardsAsteroids = "Não há nenhum asteróide perigoso hoje, fique tranquilo ;-)";

    }

    if ($update["tipo"] == 1 || $update["tipo"] == 3) {
        $hazardsAsteroids = hazardsAsteroids($dataSemHora); 
        $update["text"] = $hazardsAsteroids;
        apiRequest("sendMessage", array("text"=>$update["text"],"chat_id"=>$update["chat_id"],"parse_mode" => "html","disable_web_page_preview" => true));

    }

    if ($update["tipo"] == 2 || $update["tipo"] == 3) {
        $hazardsAsteroids = hazardsAsteroids($dataSemHora,true); 
        sendEmail($update["email"], $update["nome"], $hazardsAsteroids);

    }

    
    
}

//Mensagem de texto
if (isset($update["message"])) {
    processMessage($update["message"]);
}

//Botão de Callback
if (isset($update["callback_query"])) {
    processCallback($update["callback_query"]);

}

function sendEmail($email, $nome, $body)
{

    $mail = new PHPMailer;

    $mail->isSMTP();

    $mail->SMTPDebug = 1;

    $mail->Host = 'smtp.gmail.com';

    $mail->Port = 587;

    $mail->SMTPSecure = 'tls';

    $mail->SMTPAuth = true;

    $mail->Username = GUSERNAME;

    $mail->Password = GPASSWORD;

    $mail->setFrom('botapocalipse@asteroid.com', 'Bot Apocalipse');

    $mail->addAddress($email, $nome);

    $mail->Subject = 'Bem-vindo ao Bot Apocalipse!';

    $mail->msgHTML($body);
    $mail->CharSet = 'UTF-8';
    $mail->AltBody = 'This is a plain-text message body';

    if ($mail->send()) {
        // Section 2: IMAP
        // Salva a mensagem na pasta 'Enviados'.
        save_mail($mail);
    }

}

function requestHorario($chat_id, $tipo, $email = "")
{
    apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => "Em qual horário gostaria de receber os alertas?", "reply_markup" => array(
        'inline_keyboard' => array(
            array(
                array("text" => "01h", "callback_data" => "1h,$tipo,$email"),
                array("text" => "02h", "callback_data" => "2h,$tipo,$email"),
                array("text" => "03h", "callback_data" => "3h,$tipo,$email"),
                array("text" => "04h", "callback_data" => "4h,$tipo,$email"),
                array("text" => "05h", "callback_data" => "5h,$tipo,$email"),
                array("text" => "06h", "callback_data" => "6h,$tipo,$email"),
            ),
            array(
                array("text" => "07h", "callback_data" => "7h,$tipo,$email"),
                array("text" => "08h", "callback_data" => "8h,$tipo,$email"),
                array("text" => "09h", "callback_data" => "9h,$tipo,$email"),
                array("text" => "10h", "callback_data" => "10h,$tipo,$email"),
                array("text" => "11h", "callback_data" => "11h,$tipo,$email"),
                array("text" => "12h", "callback_data" => "12h,$tipo,$email"),
            ),
            array(
                array("text" => "13h", "callback_data" => "13h,$tipo,$email"),
                array("text" => "14h", "callback_data" => "14h,$tipo,$email"),
                array("text" => "15h", "callback_data" => "15h,$tipo,$email"),
                array("text" => "16h", "callback_data" => "16h,$tipo,$email"),
                array("text" => "17h", "callback_data" => "17h,$tipo,$email"),
                array("text" => "18h", "callback_data" => "18h,$tipo,$email"),
            ),
            array(
                array("text" => "19h", "callback_data" => "19h,$tipo,$email"),
                array("text" => "20h", "callback_data" => "20h,$tipo,$email"),
                array("text" => "21h", "callback_data" => "21h,$tipo,$email"),
                array("text" => "22h", "callback_data" => "22h,$tipo,$email"),
                array("text" => "23h", "callback_data" => "23h,$tipo,$email"),
                array("text" => "24h", "callback_data" => "0h,$tipo,$email"),
            ),
        ),
    )));
}

function requestNotificacao($chat_id)
{
    apiRequest("sendMessage", array('chat_id' => $chat_id, "text" => "Escolha o meio de notificação:", "reply_markup" => array(
        'inline_keyboard' => array(
            array(
                array("text" => "Notificação", "callback_data" => "Notificação"),
                array("text" => "E-mail", "callback_data" => "E-mail"),
            ),
            array(
                array("text" => "Notificação e E-mail", "callback_data" => "Notificação e E-mail"),
            ),
        ),
    )));
}
